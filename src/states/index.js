import { proxy } from "valtio";

export const states = proxy({
  input: {},
  selectedLink: "home",
  tools: [],
  isLoading: false,
  showMessage: false,
  message: {
    title: "",
    description: "",
  },
  user: localStorage.getItem("user"),
  showModal: false,
});

export const actions = {
  toggleLoader: () => {
    states.isLoading = !states.isLoading;
  },
};
