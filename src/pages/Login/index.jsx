import React, { useEffect, useState } from "react";
import {
  getFirestore,
  collection,
  doc,
  setDoc,
  getDoc,
} from "firebase/firestore";
import { useCollection } from "react-firebase-hooks/firestore";
import { firebaseApp } from "../../services/firebase.service";
import Input from "../../components/Input";
import { Button } from "../../components/Buttons";
import Title from "../../components/Title";
import { Link } from "react-router-dom";
import { getAuth, signInWithEmailAndPassword } from "firebase/auth";
import { actions, states } from "../../states";
import Message from "../../components/Message";
import { useSnapshot } from "valtio";

const LoginPage = (props) => {
  const { showMessage, input, isLoading } = useSnapshot(states);
  const db = getFirestore(firebaseApp);
  const [value, loading, error] = useCollection(collection(db, "tools"), {
    snapshotListenOptions: { includeMetadataChanges: true },
  });

  const [messageType, setmessageType] = useState("success");

  useEffect(() => {
    if (error) {
      console.error(error);
    }
    console.log(props);
  }, [loading]);

  const login = () => {
    const auth = getAuth();
    actions.toggleLoader();
    signInWithEmailAndPassword(auth, input.email, input.password)
      .then(async (userCredential) => {
        const userRef = doc(db, "users", userCredential.user.uid);
        const userSnap = await getDoc(userRef);

        if (userSnap.exists()) {
          localStorage.setItem(
            "user",
            JSON.stringify({ ...userSnap.data(), uid: userCredential.user.uid })
          );
          setTimeout(() => {
            window.location.reload();
          }, 3000);
        }
      })
      .catch((error) => {
        states.message = {
          title: error.code,
          description: error.message,
        };
        setmessageType("alert");
      })
      .finally(() => {
        states.showMessage = true;
        actions.toggleLoader();
      });
  };

  return (
    <div className="flex flex-row min-h-screen">
      {showMessage && <Message type={messageType} />}
      <div className="md:flex-1 bg-gray-300"></div>
      <div className="md:flex-1 flex flex-col justify-center items-center px-16">
        <Title className="font-bold text-center">
          Please enter your login credentials
        </Title>
        <span className="my-8"></span>
        <Input placeholder="Email" type="email" name="email" />
        <span className="my-2"></span>
        <Input placeholder="Password" name="password" type="password" />
        <span className="my-2"></span>
        <div className="flex justify-end items-end w-full">
          <Button type="text" className="text-xs">
            Forgot your password?
          </Button>
        </div>
        <span className="my-4"></span>
        <Button wide disabled={isLoading} loading={isLoading} onClick={login}>
          Log in
        </Button>
        <span className="my-2"></span>
        <Link to="/register">
          <span className="text-blue-600">Create an account</span>
        </Link>
      </div>
    </div>
  );
};

export default LoginPage;
