import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Button } from "../../components/Buttons";
import Input from "../../components/Input";
import Title from "../../components/Title";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";
import { useSnapshot } from "valtio";
import { actions, states } from "../../states";
import { addData } from "../../services/crud.service";
import Message from "../../components/Message";
import background from "../../assets/images/register.jpg";

const RegisterPage = () => {
  const { input, isLoading, showMessage } = useSnapshot(states);
  const [messageType, setmessageType] = useState("success");
  const register = () => {
    actions.toggleLoader();
    const auth = getAuth();
    createUserWithEmailAndPassword(auth, input.email, input.password)
      .then((userCredential) => {
        const user = userCredential.user;
        if (user) {
          addData("users", input, user.uid);
          setmessageType("success");
          states.message = {
            title: "Success",
            description: "Your account has been created.",
          };
        }
      })
      .catch((error) => {
        states.message = {
          title: error.code,
          description: error.message,
        };
        setmessageType("alert");
      })
      .finally(() => {
        actions.toggleLoader();
        states.showMessage = true;
      });
  };

  return (
    <div className="flex flex-row-reverse min-h-screen">
      {showMessage && <Message type={messageType} />}
      <div
        className="flex-1 bg-cover bg-center blur-xs"
        style={{
          backgroundImage: `url(${background})`,
        }}
      ></div>
      <div className="flex-1 flex flex-col justify-center items-center px-16">
        <Title className="font-bold text-center">Create your account</Title>
        <span className="my-8"></span>
        <Input placeholder="Nickname" name="name" />
        <span className="my-2"></span>
        <Input placeholder="Email" type="email" name="email" />
        <span className="my-2"></span>
        <Input placeholder="Password" type="password" name="password" />
        <span className="my-4"></span>
        <Button
          wide
          loading={isLoading}
          disabled={isLoading}
          onClick={register}
        >
          Create account
        </Button>
        <span className="my-2"></span>
        <Link to="/login">
          <span className="text-blue-600">
            Already have an account? Log in here
          </span>
        </Link>
      </div>
    </div>
  );
};

export default RegisterPage;
