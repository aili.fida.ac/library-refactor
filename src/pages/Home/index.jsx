import React, { useEffect, useState } from "react";
import { MainLayout } from "../../layouts/main";
import Header from "./Header";
import { states } from "../../states";
import { getFirestore, collection, doc } from "firebase/firestore";
import { useCollection, useDocument } from "react-firebase-hooks/firestore";
import { firebaseApp } from "../../services/firebase.service";
import Input from "../../components/Input";
import { Button } from "../../components/Buttons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBookmark, faPlus, faSave } from "@fortawesome/free-solid-svg-icons";
import Loader from "../../components/Loader";
import { Link } from "react-router-dom";
import { useSnapshot } from "valtio";

export const HomePage = () => {
  useEffect(() => {
    states.selectedLink = "home";
  }, []);

  return (
    <MainLayout>
      <ItemContent />
    </MainLayout>
  );
};

const ItemContent = () => {
  const { input } = useSnapshot(states);
  const [initToolState, setinitToolState] = useState([]);
  const [value, loading, error] = useCollection(
    collection(getFirestore(firebaseApp), "tools"),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  const [tools, settools] = useState([]);

  const Search = () => {
    // Search for the name and description without changing tools state
    if (input.search) {
      const newTools = initToolState.filter((tool) => {
        return (
          tool.data().name.toLowerCase().includes(input.search.toLowerCase()) ||
          tool
            .data()
            .description.toLowerCase()
            .includes(input.search.toLowerCase())
        );
      });

      settools(newTools);
    }
  };

  useEffect(() => {
    Search();
  }, [input]);

  useEffect(() => {
    if (value) {
      settools(value.docs);
      setinitToolState(value.docs);
    }

    if (error) {
    }
  }, [loading]);

  return (
    <>
      <div className="flex my-4">
        <Input
          placeholder="Search for package... Example: react"
          name="search"
        />
        <span className="mx-1"></span>

        <Link to="/form">
          <Button className="w-24 py-4">
            <FontAwesomeIcon icon={faPlus} />
          </Button>
        </Link>
      </div>
      {loading ? (
        <Loader />
      ) : (
        <table className="table-auto border-collapse border border-slate-400">
          <thead className="my-4">
            <tr>
              <th className="border border-slate-300 bg-gray-200"></th>
              <th className="border border-slate-300 py-2 bg-gray-200">Name</th>
              <th className="border border-slate-300 py-2 bg-gray-200">
                Description
              </th>
              <th className="border border-slate-300 py-2 bg-gray-200 px-4">
                Author
              </th>
              <th
                colSpan={2}
                className="border border-slate-300 py-2 bg-gray-200 px-4"
              >
                Options
              </th>
            </tr>
          </thead>
          <tbody>
            {tools.map((doc, index) => {
              return <Item key={index} doc={doc} />;
            })}
          </tbody>
        </table>
      )}
    </>
  );
};

const Item = (props) => {
  const data = props.doc.data();
  const [value, loading, error] = useDocument(
    doc(getFirestore(firebaseApp), "users", data.author),
    {
      snapshotListenOptions: { includeMetadataChanges: true },
    }
  );

  const [name, setname] = useState("");

  useEffect(() => {
    if (value) {
      setname(value.data().name);
    }

    if (error) {
      console.log(error);
    }
  }, [loading]);
  return (
    <tr>
      <td className="border border-slate-300 p-2">
        <img src={data.logo} className="w-24" alt="" />
      </td>
      <td className="border border-slate-300 p-2 text-center">
        <a href={data.link}>{data.name}</a>
      </td>
      <td className="border border-slate-300 p-2">{data.description}</td>
      <td className="border border-slate-300 p-2 text-center">
        <a href="" className="text-blue-500">
          {loading ? <Loader /> : name}
        </a>
      </td>
      <td className="text-center">
        <FontAwesomeIcon icon={faSave} />
      </td>
      <td className="text-center" onClick={() => {}}>
        <FontAwesomeIcon icon={faBookmark} />
      </td>
    </tr>
  );
};
