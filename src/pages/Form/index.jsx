import { faAdd } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { useEffect, useState } from "react";
import { Button } from "../../components/Buttons";
import Input from "../../components/Input";
import Title from "../../components/Title";
import { MainLayout } from "../../layouts/main";
import { setDoc, doc, getFirestore } from "firebase/firestore";
import { firebaseApp } from "../../services/firebase.service";
import { useSnapshot } from "valtio";
import { states } from "../../states";
import { nanoid } from "nanoid";
import Loader from "../../components/Loader";

const FormPage = () => {
  useEffect(() => {
    states.input = {};
  }, []);
  return (
    <MainLayout>
      <Title>Add a new tool</Title>
      <p>
        Lorem ipsum dolor sit, amet consectetur adipisicing elit. Obcaecati quia
        aperiam incidunt tempore eius nam repudiandae nostrum nesciunt, maiores
        esse praesentium. Quam, tempore doloremque! Illo quibusdam deserunt
        repellendus autem voluptas!
      </p>
      <div className="flex">
        <div className="flex-1 my-4">
          <FormState />
        </div>
        <div className="flex-1">
          <Preview />
        </div>
      </div>
    </MainLayout>
  );
};

const FormState = () => {
  const [loading, setloading] = useState(false);
  const { input, user } = useSnapshot(states);

  const toggleLoader = () => {
    setloading((prev) => !prev);
  };

  const addTool = async () => {
    toggleLoader();
    const _id = nanoid();
    await setDoc(doc(getFirestore(firebaseApp), "tools", _id), {
      ...input,
      author: JSON.parse(user).uid,
    });
    toggleLoader();
  };

  return (
    <div className="flex flex-col">
      <Input placeholder="Name of the package/tool" name="name" />
      <span className="my-2"></span>
      <Input
        placeholder="Little description about it"
        name="description"
        multiline
      />
      <span className="my-2"></span>
      <Input placeholder="Link to the tool (url)" name="link" />
      <span className="my-2"></span>
      <Input placeholder="Image url (logo)" name="logo" />
      <span className="my-2"></span>
      {loading ? (
        <Loader />
      ) : (
        <Button onClick={addTool}>
          <FontAwesomeIcon icon={faAdd} className="mx-2" /> Add tool
        </Button>
      )}
    </div>
  );
};

const Preview = () => {
  return (
    <div className="p-4">
      <Title type="h4" className="text-gray-700">
        Preview
      </Title>
      <p className="my-3">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laboriosam
        consectetur incidunt illum, molestias ipsum labore vitae dolores
        corporis possimus quo fugiat officiis, ratione sequi ad doloremque eum
        sapiente error debitis.
      </p>
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. A earum,
        tempore consequuntur, maxime enim aliquid dolore nisi ea nesciunt autem
        veniam, eos totam vero omnis? Voluptas molestias nisi quibusdam esse.
      </p>
    </div>
  );
};

export default FormPage;
