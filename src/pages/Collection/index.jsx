import { faPlus } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getFirestore } from "firebase/firestore";
import React from "react";
import { useSnapshot } from "valtio";
import { Button } from "../../components/Buttons";
import Input from "../../components/Input";
import Modal from "../../components/Modal";
import Title from "../../components/Title";
import { MainLayout } from "../../layouts/main";
import { addData, setuserData } from "../../services/crud.service";
import { firebaseApp } from "../../services/firebase.service";
import { states } from "../../states";

const CollectionPage = () => {
  const { user } = useSnapshot(states);
  return (
    <MainLayout>
      <Title>Your Collections</Title>
      <Modal title="Create a new collection">
        <ModalCollection />
      </Modal>
      <div className="grid grid-cols-4 my-8 gap-4">
        <AddCollectionItem />
        {JSON.parse(user).collections.map((collection) => {
          return <CollectionItem {...collection} />;
        })}
      </div>
    </MainLayout>
  );
};

const ModalCollection = () => {
  const { isLoading, user } = useSnapshot(states);
  console.log(JSON.parse(user));

  const createCollection = () => {
    addData(
      "users",
      {
        ...JSON.parse(user),
        collections: [
          ...(JSON.parse(user).collections || []),
          { name: states.input.collectionName, packages: [] },
        ],
      },
      JSON.parse(user).uid
    ).then(() => {
      setuserData(getFirestore(firebaseApp));
    });
  };

  return (
    <div className="flex flex-col">
      <Input
        name="collectionName"
        placeholder="Give a name to your new collection"
      />
      <span className="my-2"></span>
      <Button onClick={createCollection} loading={isLoading}>
        Create collection
      </Button>
    </div>
  );
};

const CollectionItem = (props) => {
  return (
    <div
      className="h-64 border-dashed border-gray-400 border-2 grid place-items-center hover:cursor-pointer"
      onClick={() => {
        states.showModal = true;
      }}
    >
      <Title type="h6">{props.name}</Title>
      <p>Packages: {props.packages.length}</p>
    </div>
  );
};

const AddCollectionItem = () => {
  return (
    <div
      className="h-64 border-dashed border-gray-400 border-2 grid place-items-center hover:cursor-pointer"
      onClick={() => {
        states.showModal = true;
      }}
    >
      <FontAwesomeIcon icon={faPlus} color="#e5e7ef" size="4x" />
    </div>
  );
};

export default CollectionPage;
