import React from "react";
import { useSnapshot } from "valtio";
import Message from "../components/Message";
import Navbar from "../components/Navbar";
import Sidebar from "../components/Sidebar";
import { states } from "../states";

export const MainLayout = ({ children }) => {
  const { showMessage, message } = useSnapshot(states);
  return (
    <div className="w-full flex">
      {showMessage && (
        <Message title={message.title} description={message.description} />
      )}
      <Sidebar />
      <main className="flex flex-col w-full md:w-4/5 p-4">
        <Navbar />
        {children}
      </main>
    </div>
  );
};
