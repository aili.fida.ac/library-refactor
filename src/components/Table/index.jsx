import React from 'react'
import DataTable from 'react-data-table-component-with-filter';

const Table = () => {
  return (
    <DataTable
      title="Companies"
      columns={columns}
      data={companies}
      pagination
    />
  );
}

export default Table