import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faSadCry } from "@fortawesome/free-regular-svg-icons";
import {
  faCubes,
  faMagnifyingGlass,
  faTelevision,
} from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";

const sidebarnavs = [
  {
    title: "Explore",
    icon: faMagnifyingGlass,
    link: "/home",
  },
  {
    title: "Collection",
    icon: faCubes,
    link: "/collection",
  },
];

const Sidebar = () => {
  return (
    <nav className="hidden md:block md:w-1/5 p-4 min-h-screen bg-gray-100">
      <ul>
        {sidebarnavs.map((nav, index) => {
          return (
            <li key={index} className="m-2">
              <Link to={nav.link}>
                <FontAwesomeIcon icon={nav.icon} />
                <span className="m-2">{nav.title}</span>
              </Link>
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default Sidebar;
