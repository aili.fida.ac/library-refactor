import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Title from "../Title";
import { useSnapshot } from "valtio";
import { states } from "../../states";

const Message = (props) => {
  const { showMessage, message } = useSnapshot(states);

  useEffect(() => {
    setTimeout(() => {
      states.showMessage = !showMessage;
    }, props.duration);
  }, []);

  const colorTypes = () => {
    switch (props.type) {
      case "success":
        return "bg-teal-200 text-green-700";
      case "warning":
        return "bg-yellow-200 text-orange-700";
      case "alert":
        return "bg-pink-200 text-red-700";
      default:
        return "bg-teal-200 text-green-200";
    }
  };

  return (
    <div
      className={`py-4 px-6 transition duration-1000 absolute z-50 left-[50%] translate-x-[-50%] ${
        showMessage ? "top-4" : "-top-20"
      } ${colorTypes()} rounded-md`}
    >
      <Title type="h6">{message.title}</Title>
      <p>{message.description}</p>
    </div>
  );
};

Message.defaultProps = {
  type: "success",
  title: "",
  description: "",
  duration: 5000,
};

Message.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string,
  duration: PropTypes.number,
  type: PropTypes.oneOf(["success", "warning", "alert"]),
};

export default Message;
