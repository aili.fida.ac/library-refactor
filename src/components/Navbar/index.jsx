import { getAuth, signOut } from "firebase/auth";
import React from "react";
import { Link } from "react-router-dom";
import { useSnapshot } from "valtio";
import { actions, states } from "../../states";
import { Button } from "../Buttons";
import Loader from "../Loader";
import Logo from "../Logo";

const links = [];

const Navbar = (props) => {
  const { selectedLink, user, showMessage, isLoading } = useSnapshot(states);

  const logout = () => {
    const auth = getAuth();
    actions.toggleLoader();
    signOut(auth)
      .then(() => {
        localStorage.removeItem("user");
        states.user = null;
        setTimeout(() => {
          window.location.reload();
        }, 1000);
      })
      .catch((error) => {
        states.message = {
          title: error.code,
          description: error.message,
        };
        setmessageType("alert");
        states.showMessage = true;
      })
      .finally(() => {
        actions.toggleLoader();
      });
  };

  return (
    <nav className="flex items-center justify-between mb-6">
      <a href="">
        <Logo />
      </a>
      <ul className="flex">
        {links.map((link) => (
          <li
            key={link.title}
            className={`mx-4 ${
              selectedLink === link.title.toLowerCase() &&
              "border-b-2 border-red-500"
            } `}
          >
            <Link to={link.link}>{link.title}</Link>
          </li>
        ))}
      </ul>
      {user ? (
        isLoading ? (
          <Loader />
        ) : (
          <Button onClick={logout}>Log out</Button>
        )
      ) : (
        <Link to="/login">
          <Button>Log in</Button>
        </Link>
      )}
    </nav>
  );
};

export default Navbar;
