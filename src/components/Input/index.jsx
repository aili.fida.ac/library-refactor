import React from "react";
import PropTypes from "prop-types";
import { states } from "../../states";

const Input = (props) => {
  return (
    <input
      {...props}
      onChange={(e) => {
        states.input[props.name] = e.target.value;
      }}
      className={`border-2 rounded-md px-2 py-2 ${props.className} ${
        props.bordercolor
      } ${props.wide && "w-full"}`}
    />
  );
};

Input.defaultProps = {
  type: "text",
  placeholder: "",
  bordercolor: "border-gray-500",
  name: "",
  wide: true,
};

// Proptypes
Input.propTypes = {
  type: PropTypes.string,
  placeholder: PropTypes.string,
  bordercolor: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  wide: PropTypes.bool,
};

export default Input;
