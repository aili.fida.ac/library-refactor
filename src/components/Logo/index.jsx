import { faCode } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React from "react";
import { Link } from "react-router-dom";

const Logo = () => {
  return (
    <Link to="/">
      LIBRAR
      <FontAwesomeIcon icon={faCode} />
    </Link>
  );
};

export default Logo;
