import React from "react";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { useSnapshot } from "valtio";
import AboutPage from "./pages/About";
import CollectionPage from "./pages/Collection";
import FormPage from "./pages/Form";
import { HomePage } from "./pages/Home";
import LoginPage from "./pages/Login";
import RegisterPage from "./pages/Register";
import { states } from "./states";

export const App = () => {
  const { user } = useSnapshot(states);
  return (
    <Router>
      <Routes>
        <Route path="/" element={user ? <HomePage /> : <LoginPage />}></Route>
        <Route path="/about" element={<AboutPage />}></Route>
        <Route path="/login" element={<LoginPage />}></Route>
        <Route path="/form" element={<FormPage />}></Route>
        <Route
          path="/collection"
          element={user ? <CollectionPage /> : <LoginPage />}
        ></Route>
        <Route path="/home" element={<HomePage />}></Route>
        <Route path="/register" element={<RegisterPage />}></Route>
      </Routes>
    </Router>
  );
};
